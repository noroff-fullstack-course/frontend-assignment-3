import { HttpErrorResponse } from '@angular/common/http';
import { Component } from '@angular/core';
import { throwError } from 'rxjs';
import { AuthLoginGuard } from './guards/auth-login.guard';
import { Trainer } from './models/trainer/trainer.model';
import { PokemonAPIService } from './services/pokemon-api.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'frontend-assignment-3';
  public trainer?: Trainer;

  constructor(
    private readonly pokeapiService: PokemonAPIService,
    public auth: AuthLoginGuard
  ) {}

  ngOnInit(): void {
    this.pokeapiService
      .loadPokemons()
      .subscribe((response) => this.pokeapiService.setPokemons(response));
  }
}
