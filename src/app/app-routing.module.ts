import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthLoginGuard } from './guards/auth-login.guard';
import { CataloguePageComponent } from './pages/catalogue-page/catalogue-page.component';
import { LoginPageComponent } from './pages/login-page/login-page.component';
import { TrainerPageComponent } from './pages/trainer-page/trainer-page.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/trainer',
  },
  {
    path: 'login',
    component: LoginPageComponent,
  },
  {
    path: 'trainer',
    component: TrainerPageComponent,
    canActivate: [AuthLoginGuard],
  },
  {
    path: 'catalogue',
    component: CataloguePageComponent,
    canActivate: [AuthLoginGuard],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
