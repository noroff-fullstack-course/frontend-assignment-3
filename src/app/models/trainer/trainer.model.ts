export class Trainer {
  public id?: number;
  public pokemon?: string[];
  public username?: string;

  constructor(id: number, pokemon: string[], username: string) {
    this.id = id;
    this.pokemon = pokemon;
    this.username = username;
  }
}
