import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';

@Injectable({
  providedIn: 'root',
})
export class PokemonAPIService {
  private API_URL: string = 'https://pokeapi.co/api/v2';

  constructor(private readonly http: HttpClient) {}

  getPokemons(): any[] {
    const pokemons = sessionStorage.getItem('pokemons');
    if (pokemons) {
      return JSON.parse(pokemons);
    } else {
      return [''];
    }
  }

  setPokemons(pokemons: any) {
    sessionStorage.setItem('pokemons', JSON.stringify(pokemons.results));
  }

  loadPokemons(): Observable<any> {
    return this.http.get<any>(this.API_URL + '/pokemon?limit=151');
  }
}
