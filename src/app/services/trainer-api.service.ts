import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, mergeAll, tap, catchError, of, isEmpty, retry } from 'rxjs';
import { Observable } from 'rxjs/internal/Observable';
import { environment } from 'src/environments/environment';
import { Trainer } from '../models/trainer/trainer.model';

@Injectable({
  providedIn: 'root',
})
export class TrainerAPIService {
  private API_URL: string;
  private API_KEY: string;
  constructor(private readonly http: HttpClient) {
    this.API_URL = environment.API_URL;
    this.API_KEY = environment.API_KEY;
  }

  getTrainers(): Observable<Trainer[]> {
    return this.http
      .get<Trainer[]>(this.API_URL + '/trainers')
      .pipe(catchError(this.handleError<Trainer[]>('getTrainers')));
  }

  getTrainer(name: string): Observable<Trainer[]> {
    return this.http
      .get<Trainer[]>(this.API_URL + `/trainers?username=${name}`)
      .pipe(catchError(this.handleError<Trainer[]>('getTrainer')), retry(3));
  }

  createTrainer(name: string) {
    const headers = {
      'X-API-KEY': this.API_KEY,
      'Content-Type': 'application/json',
    };
    const body = {
      username: name,
      pokemon: [],
    };
    return this.http.post(this.API_URL + '/trainers', JSON.stringify(body), {
      headers,
    });
  }

  addPokemon(pokemonName: string) {
    const trainerString = sessionStorage.getItem('trainer');
    let trainer = {} as Trainer;

    if (trainerString) {
      trainer = JSON.parse(trainerString);
    }

    trainer.pokemon?.push(pokemonName);

    const header = {
      'X-API-KEY': this.API_KEY,
      'Content-Type': 'application/json',
    };

    const body = {
      pokemon: trainer.pokemon,
    };
    return this.http.patch(
      this.API_URL + '/trainers/' + trainer.id,
      JSON.stringify(body),
      { headers: header }
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      alert(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
