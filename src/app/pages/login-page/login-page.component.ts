import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Trainer } from 'src/app/models/trainer/trainer.model';
import { TrainerAPIService } from 'src/app/services/trainer-api.service';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css'],
})
export class LoginPageComponent {
  constructor(
    private readonly apiService: TrainerAPIService,
    private router: Router
  ) {}

  public trainer = {} as Trainer;

  public login(form: NgForm) {
    const username = form.value.loginName;
    if (username) {
      this.apiService.getTrainer(username).subscribe((response) => {
        if (response[0]) {
          // IF trainer exists,
          this.trainer = response[0];
          console.log(this.trainer);
          sessionStorage.setItem('trainer', JSON.stringify(this.trainer));
          this.router.navigate(['/trainer']);
        } else {
          // IF trainer doesnt exist, create a new
          this.apiService.createTrainer(username).subscribe((response) => {
            this.trainer = response;
            sessionStorage.setItem('trainer', JSON.stringify(this.trainer));
            this.router.navigate(['/trainer']);
          });
        }
      });
    }
  }
}
