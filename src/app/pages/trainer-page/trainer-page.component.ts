import { Component } from '@angular/core';
import { Trainer } from 'src/app/models/trainer/trainer.model';
import { PokemonAPIService } from 'src/app/services/pokemon-api.service';

@Component({
  selector: 'app-trainer-page',
  templateUrl: './trainer-page.component.html',
  styleUrls: ['./trainer-page.component.css'],
})
export class TrainerPageComponent {
  public trainer = {} as Trainer;

  constructor(private readonly pokeapiService: PokemonAPIService) {
    const trainerJson = sessionStorage.getItem('trainer');
    if (trainerJson) {
      this.trainer = JSON.parse(trainerJson);
    }
  }
}
