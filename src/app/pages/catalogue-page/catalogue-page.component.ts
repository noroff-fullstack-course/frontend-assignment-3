import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { PokemonAPIService } from 'src/app/services/pokemon-api.service';
import { TrainerAPIService } from 'src/app/services/trainer-api.service';

@Component({
  selector: 'app-catalogue-page',
  templateUrl: './catalogue-page.component.html',
  styleUrls: ['./catalogue-page.component.css'],
})
export class CataloguePageComponent {
  constructor(
    private readonly pokeapiService: PokemonAPIService,
    private readonly trainerapiService: TrainerAPIService
  ) {}

  public pokemons = this.pokeapiService.getPokemons();

  public onSubmit(pokemon: string) {
    this.trainerapiService
      .addPokemon(pokemon)
      .subscribe((response) =>
        sessionStorage.setItem('trainer', JSON.stringify(response))
      );
    alert(`You just caught ${pokemon}!`);
  }
}
